##Установка необходимых библиотек

`sudo apt-get install snmp`

`sudo apt-get install libsnmp-dev`

##Конфигурация роутера 

**Mikrotik**

1. Необходимо зайти в mikrotik через ssh. Стандартный логин admin, без пароля

2. Проверяем snmp. 

	`snmp print`
	
	Если указано *enabled: no*, включаем snmp
	
	`snmp set enabled=yes`
	
3. Проверяем community
	
	`snmp community print value-list`
	
	Если создана только public community, создаем новую
	
	`snmp community add name=<CommunityName> security=authorized authentication-password=<CommunityPassword>`
	
Подробнее по [ссылке](https://wiki.mikrotik.com/wiki/Manual:SNMP)
	
##OIDы

**Основные OIDы (MikroTik)**

*	Напряжение: `.1.3.6.1.4.1.14988.1.1.3.8.0`

*	Температура: `.1.3.6.1.4.1.14988.1.1.3.10.0`

*	Сила сигнала rx: `.1.3.6.1.4.1.14988.1.1.1.2.1.3.<Decimal MAC adress>.6`

	*Например, для MAC:* `54:14:73:C4:EB:D5` 
	
	*OID будет иметь вид* `.1.3.6.1.4.1.14988.1.1.1.2.1.3.84.20.115.196.235.213.6`

*	Сила сигнала tx: `.1.3.6.1.4.1.14988.1.1.1.2.1.19.<Decimal MAC adress>.6`

**Как узнать другие OIDы (MikroTik)**

1. Подсоединиться к mikrotiky через ssh. Стандартный логин *admin*, без пароля

2. Прописать нужный путь + `print oid`. 

	Например, информация о wifi подключениях (сила сигнала, время и т.д.):

	`interface wireless registration-table print oid`
		
